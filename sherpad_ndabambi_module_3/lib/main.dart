import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(const MyApp());
}

const String title = "Assignment Tracker";

Map<int, Color> color = {
  50: const Color.fromRGBO(24, 50, 39, .1),
  100: const Color.fromRGBO(24, 50, 39, .2),
  200: const Color.fromRGBO(24, 50, 39, .3),
  300: const Color.fromRGBO(24, 50, 39, .4),
  400: const Color.fromRGBO(24, 50, 39, .5),
  500: const Color.fromRGBO(24, 50, 39, .6),
  600: const Color.fromRGBO(24, 50, 39, .7),
  700: const Color.fromRGBO(24, 50, 39, .8),
  800: const Color.fromRGBO(24, 50, 39, .9),
  900: const Color.fromRGBO(24, 50, 39, 1),
};

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const SignInPage(),
      title: title,
      theme: ThemeData(
        primarySwatch: MaterialColor(0xff18323d, color),
      ),
    );
  }
}

class SignInPage extends StatelessWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text(title)),
        backgroundColor: const Color(0xffc0e0f0),
        body: Column(
          children: [
            Row(
              children: const [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Icon(
                      Icons.checklist_rtl,
                      size: 64,
                    ),
                  ),
                ),
              ],
            ),
            const Text(
              "Sign in to $title",
              textScaleFactor: 2,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(64, 48, 64, 0),
              child: Column(
                children: [
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Enter your email address',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: const BorderSide(color: Color(0xff18323d)),
                      ),
                    ),
                  ),
                  const SizedBox(height: 32),
                  TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Enter your password',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: const BorderSide(color: Color(0xff18323d)),
                      ),
                    ),
                    obscureText: true,
                  ),
                  const SizedBox(height: 32),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Dashboard()),
                      );
                    },
                    child: const Text('Sign in'),
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 32, vertical: 24),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        )),
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  const Text("Don't have an account?"),
                  const SizedBox(
                    height: 16,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SignUpPage()),
                      );
                    },
                    child: const Text("Sign up"),
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 32, vertical: 24),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }
}

class SignUpPage extends StatelessWidget {
  const SignUpPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text(title)),
      backgroundColor: const Color(0xffc0e0f0),
      body: Column(
        children: [
          Row(
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Icon(
                    Icons.checklist_rtl,
                    size: 64,
                  ),
                ),
              ),
            ],
          ),
          const Text(
            "Sign up for $title",
            textScaleFactor: 2,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(64, 16, 64, 0),
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Enter your first name',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Enter your last name',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Enter your email address',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Choose a password',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xff18323d),
                      ),
                    ),
                  ),
                  obscureText: true,
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Confirm your password',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xff18323d),
                      ),
                    ),
                  ),
                  obscureText: true,
                ),
                const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Dashboard(),
                      ),
                    );
                  },
                  child: const Text('Sign up'),
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 32, vertical: 24),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Dashboard extends StatelessWidget {
  const Dashboard({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(title),
      ),
      backgroundColor: const Color(0xffc0e0f0),
      body: ListView.separated(
        padding: const EdgeInsets.all(8),
        itemCount: assignments.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            iconColor: const Color(0xffc0e0f0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            textColor: const Color(0xffc0e0f0),
            tileColor: const Color(0xff18323d),
            title: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 0),
              child: ListTile(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                ),
                tileColor: const Color(0xffc0e0f0),
                title: Text(assignments[index].name),
                subtitle: Text(assignments[index].subject),
              ),
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    const Text("Due:\t\t\t"),
                    Text(DateFormat.yMMMd()
                        .add_Hm()
                        .format(assignments[index].dueDate)),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Scope:\t"),
                    Text(assignments[index].scope),
                  ],
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: ElevatedButton(
                      onPressed: () {
                        assignments.removeAt(index);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Dashboard()),
                        );
                      },
                      child: Row(
                        children: const [
                          Icon(
                            Icons.check,
                            size: 14,
                          ),
                          Text("Mark as done"),
                        ],
                      ),
                      style: ElevatedButton.styleFrom(
                          fixedSize: const Size(110, 0),
                          primary: Colors.green,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                          textStyle: const TextStyle(
                            fontSize: 10,
                          )),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(top: 80),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FloatingActionButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const EditProfile()),
                );
              },
              backgroundColor: Colors.grey,
              child: const Icon(Icons.account_circle),
            ),
            FloatingActionButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddAssignment()));
              },
              backgroundColor: Colors.green,
              child: const Icon(Icons.add),
            )
          ],
        ),
      ),
    );
  }
}

class AddAssignment extends StatelessWidget {
  AddAssignment({super.key});

  final nameController = TextEditingController();
  final subjectController = TextEditingController();
  final dueDateController = TextEditingController();
  final dueTimeController = TextEditingController();
  final scopeController = TextEditingController();

  void dispose() {
    nameController.dispose();
    subjectController.dispose();
    dueDateController.dispose();
    dueTimeController.dispose();
    scopeController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(title),
      ),
      backgroundColor: const Color(0xffc0e0f0),
      body: Column(
        children: [
          Row(
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Icon(
                    Icons.checklist_rtl,
                    size: 64,
                  ),
                ),
              ),
            ],
          ),
          const Text(
            "Add an assignment",
            textScaleFactor: 2,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(64, 16, 64, 0),
            child: Column(
              children: [
                TextFormField(
                  controller: nameController,
                  decoration: const InputDecoration(
                    labelText: 'Assignment name',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: subjectController,
                  decoration: const InputDecoration(
                    labelText: 'Subject',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: dueDateController,
                  decoration: const InputDecoration(
                    labelText: 'Due date (YYYY-MM-DD)',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xff18323d),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: dueTimeController,
                  decoration: const InputDecoration(
                    labelText: 'Due time (HH:MM)',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xff18323d),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  controller: scopeController,
                  decoration: const InputDecoration(
                    labelText: 'Scope',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xff18323d),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: () {
                    assignments.add(
                      Assignment(
                        name: nameController.text,
                        subject: subjectController.text,
                        dueDate: DateTime.parse(
                            '${dueDateController.text} ${dueTimeController.text}'),
                        scope: scopeController.text,
                      ),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const Dashboard()),
                    );
                  },
                  child: const Text('Done'),
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 32, vertical: 24),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class EditProfile extends StatelessWidget {
  const EditProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(title),
      ),
      backgroundColor: const Color(0xffc0e0f0),
      body: Column(
        children: [
          Row(
            children: const [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Icon(
                    Icons.account_circle,
                    size: 64,
                  ),
                ),
              ),
            ],
          ),
          const Text(
            "Edit profile",
            textScaleFactor: 2,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(64, 16, 64, 0),
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Enter new first name',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Enter new last name',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Enter new email address',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xff18323d)),
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Enter new password',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xff18323d),
                      ),
                    ),
                  ),
                  obscureText: true,
                ),
                const SizedBox(height: 16),
                TextFormField(
                  decoration: const InputDecoration(
                    labelText: 'Confirm new password',
                    border: UnderlineInputBorder(),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color(0xff18323d),
                      ),
                    ),
                  ),
                  obscureText: true,
                ),
                const SizedBox(height: 48),
                ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Save changes'),
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 32, vertical: 24),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Assignment {
  String name;
  String subject;
  DateTime dueDate;
  String scope;

  Assignment(
      {required this.name,
      required this.subject,
      required this.dueDate,
      required this.scope});
}

List<Assignment> assignments = <Assignment>[
  Assignment(
    name: "Assessment 3",
    subject: "COS1511: Introduction to Programming I",
    dueDate: DateTime.parse("2022-06-13 20:00"),
    scope: "Study guide lessons 1-29",
  ),
  Assignment(
    name: "Assessment 2",
    subject: "COS1521: Computer Systems: Fundamental Concepts",
    dueDate: DateTime.parse("2022-06-13 21:00"),
    scope: "Forouzan Chapters 5 to 7"
        "\nTutorial letter 102 units 5 to 7",
  ),
  Assignment(
    name: "Assessment 2",
    subject: "COS1501: Theoretical Computer Science I",
    dueDate: DateTime.parse("2022-06-14 21:00"),
    scope: "Study units 4 to 6.3",
  ),
];
